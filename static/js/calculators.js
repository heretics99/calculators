function isNumber(n){
return typeof n == 'number' && !isNaN(n - n);
}
function addCommaOnly(x) {
	x = parseFloat(x)
	if (isNumber(x))
	{
		sign = 1
		if (x<0)
		{
			sign = -1
			x *= sign
		}
		f_str = sign==1 ? "" : "-"
	    var parts = x.toString().split(".");
	    if (parts[0].length > 3)
		    parts[0] = parts[0].slice(0,-3).replace(/\B(?=(\d{2})+(?!\d))/g, ",")+","+parts[0].slice(-3)
	    f_str += parts.join(".");
	    return(f_str)
	}
	else return x.toString()
}
function addCommaCurrency(x) {
	x = parseFloat(x)
	if (isNumber(x))
	{
		return "Rs "+addCommaOnly(x)
	}
	else return x.toString()
}	
function convertToNum(n){
	if (!isNumber(parseFloat(n))) return 0
	else return n
}
$(document).ready(function() {
	$(function() {
	  $("#principal").focus();
	});	
	function annualRecurringDepositCalculator(principal, rate, time_years, time_months, frequency)
	{
		months_in_year = 12.0
		sum = 0
		total_p = 0
		p = parseFloat(principal)
		r = parseFloat(rate)
		n_months = parseFloat(time_months)
		if (isNaN(n_months - n_months)) n_months = 0
		n_years = parseFloat(time_years)
		if (isNaN(n_years - n_years)) n_years = 0
		n = n_years + n_months/months_in_year
		n_floor = Math.floor(n)
		n_months = Math.floor((n-n_floor)*months_in_year)
		n = n_floor + n_months/months_in_year
		n_ceil = Math.ceil(n)
		f = parseInt(frequency)
		delta = months_in_year/f
		for (var i=n_ceil; i > 0; i--) 
		{
		    t = p*Math.pow((1+((r/delta)/100.0)), (n-n_ceil+i)*delta)
		    sum += t
		    total_p += p
		    row = '<tr><td>'+addCommaCurrency(p.toFixed(2))+'</td><td>'+(n_ceil-i)+'</td><td>'+n_floor+"."+n_months+
		    		'</td><td>'+(i+n_floor-n_ceil)+"."+n_months+'</td><td>'+addCommaCurrency(t.toFixed(2))+
		    		'</td><td>'+addCommaCurrency(sum.toFixed(2))+'</td></tr>'
		    $('#interest_table > tbody:last').append(row);
		};
	    row = '<tr class="success"><td colspan="5" class="cellTextRight">Total Maturity Amount:</td><td class="textMajor">'+addCommaCurrency(sum.toFixed(2))+'</td></tr>'
	    $('#interest_table > tbody:last').append(row);

		$("#breakup_details").show()
		$("#label4").text("Installment: "+addCommaCurrency(p.toFixed(2))+"")
		$("#label5").text("Rate of Interest: "+r.toFixed(2)+"%")
		$("#label6").text("Time: "+n_floor+" Years and "+n_months+" Months")
		$("#label1").text("Maturity amount: "+addCommaCurrency(sum.toFixed(2))+"")
		$("#label2").text("Interest: "+addCommaCurrency((sum-total_p).toFixed(2))+"")
		$("#label3").text("Principal: "+addCommaCurrency((total_p).toFixed(2))+"")
	}

	$("#submit_btn.annual-recurring").click(function() {
		$("#interest_table").find("tr:gt(0)").remove();
		principal = $("#principal").val()
		rate = $("#rate").val()
		time_years = $("#time_years").val()
		time_months = $("#time_months").val()
		frequency = $("input:radio[name=compoundperiod]:checked").val()
	  	annualRecurringDepositCalculator(principal, rate, time_years, time_months, frequency)
	});
	
	function monthlyRecurringDepositCalculator(principal, rate, time_years, time_months, frequency)
	{
		months_in_year = 12.0
		sum = 0
		total_p = 0
		p = parseFloat(principal)
		r = parseFloat(rate)

		n_months = parseFloat(time_months)
		if (isNaN(n_months - n_months)) n_months = 0
		n_years = parseFloat(time_years)
		if (isNaN(n_years - n_years)) n_years = 0
		n = Math.floor(n_years*months_in_year + n_months) //months
		n_years = Math.floor(n_floor / months_in_year)
		n_months = n % months_in_year

		f = parseInt(frequency)
		delta = months_in_year/f
		for (var i=n; i > 0; i--) 
		{
		    t = p*Math.pow((1+((r/delta)/100.0)), (i)*(delta/months_in_year))
		    sum += t
		    total_p += p
		    row = '<tr><td>'+addCommaCurrency(p.toFixed(2))+'</td><td>'+(n-i)+'</td><td>'+n+
		    		'</td><td>'+parseFloat((i+n-n_ceil).toFixed(2)).toString()+'</td><td>'+addCommaCurrency(t.toFixed(2))+
		    		'</td><td>'+addCommaCurrency(sum.toFixed(2))+'</td></tr>'
		    $('#interest_table > tbody:last').append(row);
		};
	    row = '<tr class="success"><td colspan="5" class="cellTextRight">Total Maturity Amount:</td><td class="textMajor">'+addCommaCurrency(sum.toFixed(2))+'</td></tr>'
	    $('#interest_table > tbody:last').append(row);

		$("#breakup_details").show()
		$("#label4").text("Installment: "+addCommaCurrency(p.toFixed(2))+"")
		$("#label5").text("Rate of Interest: "+r.toFixed(2)+"%")
		$("#label6").text("Time: "+n_floor+" Years and "+n_months+" Months")
		$("#label1").text("Maturity amount: "+addCommaCurrency(sum.toFixed(2))+"")
		$("#label2").text("Interest: "+addCommaCurrency((sum-total_p).toFixed(2))+"")
		$("#label3").text("Principal: "+addCommaCurrency((total_p).toFixed(2))+"")
	}


	$("#submit_btn.monthly-recurring").click(function() {
		$("#interest_table").find("tr:gt(0)").remove();
		principal = $("#principal").val()
		rate = $("#rate").val()
		time_years = $("#time_years").val()
		time_months = $("#time_months").val()
		frequency = $("input:radio[name=compoundperiod]:checked").val()
	  	monthlyRecurringDepositCalculator(principal, rate, time_years, time_months, frequency)
	});
	
	function emiCalculator(principal, rate, time_years, time_months, emi, findwhat)
	{
		if(findwhat=="") findwhat="findemi";
		months_in_year = 12.0;
		total_principal = 0.0;
		total_interest = 0.0;
		p = Math.abs(parseFloat(principal))
		if (isNaN(p - p)) p = 0;
		rate = Math.abs(parseFloat(rate))
		if (isNaN(rate - rate)) rate = 0;
		r = rate/(100.0*months_in_year)
		n_months = Math.abs(parseFloat(time_months))
		if (isNaN(n_months - n_months)) n_months = 0;
		n_years = Math.abs(parseFloat(time_years))
		if (isNaN(n_years - n_years)) n_years = 0;
		n = Math.floor(n_years*months_in_year + n_months) //months
		emi = Math.abs(parseFloat(emi));
		if (isNaN(emi - emi)) emi = 0;

		n_years = Math.floor(n/months_in_year)
		n_months = n % months_in_year
		$("#time_years").val(n_years);
		$("#time_months").val(n_months);


		if(findwhat=="findemi")
		{
			if (r==0)
			{
				emi = p/n
				p_remaining = p
			}
			else
			{
				tt = 1+r;
				tmp = Math.pow(tt,n);
				emi = p*r*tmp / (tmp - 1);
				p_remaining = p;
			}
			if (isNaN(emi - emi)) emi = 0;
			if(emi==0) emi = p_remaining;
			// console.log(emi);
			$("#emi").val(emi.toFixed(2));
		}
		if(findwhat=="findprincipal")
		{
			if (r==0)
			{
				p = emi*n
				p_remaining = p
			}
			else
			{
				tt = 1+r;
				tmp = Math.pow(tt,n);
				p = emi/(r*tmp / (tmp - 1));
				p_remaining = p;
			}
			$("#principal").val(p.toFixed(2));
		}
		if(findwhat=="findrate")
		{
			if (r==0)
			{
				emi = p/n
				p_remaining = p
			}
			else
			{
				tt = 1+r;
				tmp = Math.pow(tt,n);
				emi = p*r*tmp / (tmp - 1);
				p_remaining = p;
			}
			$("#emi").val(emi.toFixed(2));			
		}
		if(findwhat=="findtime")
		{
			console.log(emi);
			console.log(p);
			console.log(r);

			max_r = emi/p*.98;
			min_r = 0.000001;
			n=0
			if(r>min_r && r<max_r)
			{
				n = (Math.log(emi)-Math.log(emi-p*r))/(Math.log(1+r));
				n = Math.ceil(n);
			}
			// console.log(temp);
			else
			{
				$("#p-error-msg").parent().show();
				$("#p-error-msg").text("Rate of interest should be between "+min_r*1200+" and "+max_r*1200);
			}
			// if (r==0)
			// {
			// 	emi = p/n
			// 	p_remaining = p
			// }
			// else
			// {
			// 	tt = 1+r;
			// 	tmp = Math.pow(tt,n);
			// 	emi = p*r*tmp / (tmp - 1);
			// 	p_remaining = p;
			// }
			// n= temp;
			p_remaining = p;
	
			n_years = Math.floor(n/months_in_year)
			n_months = n % months_in_year

			$("#time_years").val(n_years);
			$("#time_months").val(n_months);
		}

		console.log("Time::"+n);
		for (var i=0; i <= n; i++) 
		{
		    if (i == 0)
		    {
			    row = '<tr><td></td><td>0</td><td colspan="3" class="cellTextCenter"> -- </td><td>'+addCommaCurrency(p_remaining.toFixed(2))+'</td></tr>'
			    $('#interest_table > tbody:last').append(row);
			    continue
			}
		    int_comp = p_remaining*r
		    principal_comp = emi - int_comp
		    p_remaining = p_remaining + int_comp - emi
		    total_interest += int_comp
		    total_principal += principal_comp
		    // if(p_remaining<emi)
		    // 	emi=p_remaining;
		    x = Math.floor(i/months_in_year)
		    year_frac = i%months_in_year
		    row = '<tr><td>'+addCommaCurrency(emi.toFixed(2))+'</td><td>'+(i)+'</td><td>'+x+"."+year_frac+
		    		'</td><td>'+addCommaCurrency(int_comp.toFixed(2))+'</td><td>'+addCommaCurrency(principal_comp.toFixed(2))+
		    		'</td><td>'+addCommaCurrency(p_remaining.toFixed(2))+'</td></tr>'
		    $('#interest_table > tbody:last').append(row);
		    if (p_remaining<0 || principal_comp<0)
		    {
		    	break;
		    }
		};
		if (isNaN(emi - emi)) emi = 0;
		i_p_ratio = total_interest/total_principal;
		if (isNaN(i_p_ratio - i_p_ratio)) i_p_ratio = 0;

	    row = '<tr class="success"><td colspan="5" class="cellTextRight">Remaining Principal:</td><td class="textMajor">'+addCommaCurrency(p_remaining.toFixed(2))+'</td></tr>'
	    $('#interest_table > tbody:last').append(row);


		$("#breakup_details").show()
		$("#label4").text("Loan Amount: "+addCommaCurrency(p.toFixed(2))+"")
		$("#label5").text("Rate of Interest: "+rate.toFixed(2)+"%")
		$("#label6").text("Time: "+n_years+" Years and "+n_months+" Months")
		$("#label1").text("EMI: "+addCommaCurrency(emi.toFixed(2))+"")
		$("#label2").text("Total Interest: "+addCommaCurrency(total_interest.toFixed(2))+"")
		$("#label3").text("Interest to Principal Ratio : "+addCommaOnly((i_p_ratio).toFixed(5))+"")

	}

//EMI Page
	
	$("input[name='calcoptions']").change(function(event) {
		console.log("event fired");
		$( "#submit_btn.emi-calculator" ).trigger( "click" );
	});
	
	$("#submit_btn.emi-calculator").click(function() {
		$("#interest_table").find("tr:gt(0)").remove();
		$("#p-error-msg").parent().hide();
		var elemPrincipal = $("#principal");
		var elemRate = $("#rate");
		var elemTimeYears = $("#time_years");
		var elemTimeMonths = $("#time_months");
		var elemEmi = $("#emi");

		var principal = elemPrincipal.val();
		var rate = elemRate.val();
		var time_years = elemTimeYears.val();
		var time_months = elemTimeMonths.val();
		var emi = elemEmi.val();

		var findwhat = "";
		var findwhatSelected = $("input[type='radio'][name='calcoptions']:checked");
		if (findwhatSelected.length>0)
		{
			findwhat = findwhatSelected.val();
			console.log(findwhat);
		}
		switch(findwhat)
		{
			case "findemi":
			elemEmi.attr("disabled", "disabled");
			elemPrincipal.removeAttr("disabled");
			elemRate.removeAttr("disabled");
			elemTimeYears.removeAttr("disabled");
			elemTimeMonths.removeAttr("disabled");
			break;
			case "findprincipal":
			elemPrincipal.attr("disabled", "disabled");
			elemEmi.removeAttr("disabled");
			elemRate.removeAttr("disabled");
			elemTimeYears.removeAttr("disabled");
			elemTimeMonths.removeAttr("disabled");
			break;
			case "findrate":
			elemRate.attr("disabled", "disabled");
			elemPrincipal.removeAttr("disabled");
			elemEmi.removeAttr("disabled");
			elemTimeYears.removeAttr("disabled");
			elemTimeMonths.removeAttr("disabled");
			break;
			case "findtime":
			elemTimeYears.attr("disabled", "disabled");
			elemTimeMonths.attr("disabled", "disabled");
			elemPrincipal.removeAttr("disabled");
			elemRate.removeAttr("disabled");
			elemEmi.removeAttr("disabled");
			break;

		}
	  	emiCalculator(principal, rate, time_years, time_months, emi, findwhat)
	});
	
	$("#reset_btn").click(function() {
		this.form.reset()
		$("#breakup_details").hide()
	});
	
	$("#print_btn.recurring-deposit").click(function() {
		$("#calculator_div").append("<div id='printable_area'></div>")
		$("#printable_area").append($("#calculator_heading").clone())
		$("#printable_area").append("<hr/><div>Recurring Principal: "+addCommaCurrency($("#principal").val())
				+"<br/> Rate of Interest: "+$("#rate").val()+"%<br/> Time (years): "+$("#time").val()+"</div><hr/>")
		$("#printable_area").append("<div class='textMajor'>"+$("#label1").text()
				+"<br/>"+$("#label2").text()+"<br/>"+$("#label3").text()+"</div><hr/>")
		$("#printable_area").append("<h3>Breakup Chart</h3>")
		$("#printable_area").append($("#interest_table").clone().addClass("printTextSize"))
		window.print()
		$("#printable_area").remove()
	});
	
	$("#print_btn.emi-calculator").click(function() {
		$("#calculator_div").append("<div id='printable_area'></div>")
		$("#printable_area").append($("#calculator_heading").clone())
		$("#printable_area").append("<hr/><div>Loan Amount: "+addCommaCurrency($("#principal").val())
				+"<br/> Rate of Interest: "+$("#rate").val()+"%<br/> Loan Term: "+convertToNum($("#time_years").val())
				+" Years, "+convertToNum($("#time_months").val())+" Months</div><hr/>")
		$("#printable_area").append("<div class='textMajor'>"+$("#label1").text()
				+"<br/>"+$("#label2").text()+"<br/>"+$("#label3").text()+"</div><hr/>")
		$("#printable_area").append("<h3>Breakup Chart</h3>")
		$("#printable_area").append($("#interest_table").clone().addClass("printTextSize"))
		window.print()
		$("#printable_area").remove()
	});
	
});
