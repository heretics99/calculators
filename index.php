<?php 
include 'topnav.php'
?>
<script src="static/js/calculators.js"></script>
<link href="static/css/calculator_css.css" rel="stylesheet" />
<title>Easy EMI | Recurring Deposits Calulator </title>
    <div class="container">

      <!-- Example row of columns -->
      <div class="row atleast-minheight">
        <div class="span9" id="calculator_div">
        	<h2 align="center" id="calculator_heading">Choose Calculator</h2>
			<br />
			<br />
			<p class="lead">Just yesterday (7th October 2012), I was signing up for a LIC Jeevan Saral plan. I needed to perform some 
				quick calculation to select the best recurring deposit (RD), based on ROI. I tried searching for some good online 
				utility which can serve the purpose. To my surprise, out of several available online calculators,
				there was not a single one which was presentable enough. None of them provide breakup details etc. I decided
				to create one myself and make it publicly available so that others can use it as well.
				<h4>Features:</h4>
					<ul style="font-size:17px">
						<li>Cleaner user interface. No clutter.</li>
						<li>Provides accurate calculations and 2 decimal precision.</li>
						<li>Time (in years or months) can be in fractions.</li>
						<li>Rate of interest can be above 100.</li>
						<li>Printer friendly print option.</li>
					</ul>
			</p>
			<h4>Choose calculator:</h4>
			<ul style="font-size:34px">
				<a href="emi_calculator.php" class="btn btn-large"><abbr title="Calculate EMI for a loan amount">Loan EMI Calculator</abbr></a>
				<a href="annual_recurring.php" class="btn btn-large"><abbr title="Yearly deposits of a fixed amount">Annual Recurring Deposit</abbr></a>
				<a href="monthly_recurring.php" class="btn btn-large"><abbr title="Monthly deposits of a fixed amount">Monthly Recurring Deposit</abbr></a>
			</ul>
			<br/>
			<br/>
			
        </div>
        <div class="span3">
        	<? include 'more_calculators.php' ?>
       </div>
      </div>
    
	<? include 'footer.php'; ?>
    </div> <!-- /container -->

