  <h2>More Calculators</h2>
  <div class="btn-group btn-group-vertical">
	<a href="emi_calculator.php" class="btn btn-large"><abbr title="Calculate EMI for a loan amount">Loan EMI Calculator</abbr></a>
	<a href="annual_recurring.php" class="btn btn-large"><abbr title="Yearly deposits of a fixed amount">Annual Recurring Deposit</abbr></a>
	<a href="monthly_recurring.php" class="btn btn-large"><abbr title="Monthly deposits of a fixed amount">Monthly Recurring Deposit</abbr></a>
  </div>
<p></p>
