<? $link = basename($_SERVER['PHP_SELF']) ?>
<? include 'base.php'; ?>
<body>
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="index.php">Calculators</a>
          <div class="nav-collapse collapse">
             <ul class="nav">
				<li <?echo $link == "emi_calculator.php"?'class="active"':''?>><a href="emi_calculator.php">EMI Calculator</a></li>
				<li <?echo $link == "annual_recurring.php"?'class="active"':''?>><a href="annual_recurring.php">Annual Recurring Deposit</a></li>
				<li <?echo $link == "monthly_recurring.php"?'class="active"':''?>><a href="monthly_recurring.php">Monthly Recurring Deposit</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
    
<hr>
