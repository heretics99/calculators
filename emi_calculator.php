<?php 
include 'topnav.php'
?>
<script src="static/js/calculators.js"></script>
<script>
$(document).ready(function(){
	$('.alert .close').live("click", function(e) {
	    $(this).parent().hide();
	});
});
</script>
<link href="static/css/calculator_css.css" rel="stylesheet" />
<title>EMI Calculator</title>
    <div class="container">

      <!-- Example row of columns -->
      <div class="row atleast-minheight">
        <div class="span9" id="calculator_div">
        	<h2 align="center" id="calculator_heading">EMI Calculator</h2>
			<br />
			<br />
			<form class="form-horizontal">
				<div class="control-group">
				    <label class="control-label" for="principal">Loan Amount</label>
				    <div class="controls">
				      <input type="text" id="principal" placeholder="Loan Amount">
				      <label class="radio inline"><input type="radio" name="calcoptions" id="radiotime" value="findprincipal"> Find Principal</radio>
				    </div>
				</div>

				<div class="control-group">
				    <label class="control-label" for="rate">Rate of Interest</label>
				    <div class="controls">
				      <input type="text" id="rate" placeholder="% Rate of Interest">
				      <label class="radio inline"><input type="radio" name="calcoptions" id="radiorate" value="findrate"> Find Interest Rate</radio>
				      <span class="help-inline"></span>
				    </div>
				</div>

				<div class="control-group">
				    <label class="control-label" for="time">Loan Term</label>
				    <div class="controls">
				      <input type="text" id="time_years" placeholder="Years" class="input-small">
				      <input type="text" id="time_months" placeholder="and Months" class="input-small">
				      <span class="help-inline"></span>
				      <label class="radio inline"><input type="radio" name="calcoptions" id="radiotime" value="findtime"> Find Loan Term</radio>
				    </div>
				</div>
				<div class="control-group">
				    <label class="control-label" for="emi">EMI</label>
				    <div class="controls">
				      <input type="text" id="emi" placeholder="Equated Monthly Instalment" disabled>
				      <label class="radio inline"><input type="radio" name="calcoptions" id="radioemi" value="findemi" checked> Find EMI</radio>
				    </div>
				</div>
				<div class="control-group">
					<label class="control-label" ></label>
				    <div class="actions">
				      <button class="btn btn-primary emi-calculator" id="submit_btn" type="submit" onclick="return false;">Calculate</button>
				      <button class="btn" id="reset_btn" type="button" onclick="return false;">Reset</button>
				    </div>
				</div>
				<div class="alert alert-error" style="display: none">
				  <a class="close" href="#">×</a>
				  <p id="p-error-msg"></p>
				</div>
            </form>
			<div id="breakup_details" style="display: none">
				<hr/>
					<div class="well textMajor" id="raw_details">
				    <span class="" id="label4"></span> &nbsp;|&nbsp;
				    <span class="" id="label5"></span> &nbsp;|&nbsp;
				    <span class="" id="label6"></span>
				    </div>
					<div class="alert alert-success" id="brief_details">
				    <span class="badge badge-info bigtext textMajor" id="label1"></span> &nbsp;
				    <span class="badge badge-success bigtext" id="label2"></span> &nbsp;
				    <span class="badge badge-warning bigtext" id="label3"></span>
				    </div>
				<hr/>
				<div id="feature_buttons" class="pull-right button-padding">
				<button class="btn btn-primary emi-calculator" id="print_btn" type="button">Print Chart</button>
				<!-- <button class="btn btn-primary" id="save_btn" type="button" onclick="return false;">Save as Excel Sheet</button> -->
				</div>
				<h3>Breakup Chart</h3>
				<table class="table table-hover table-bordered table-condensed" id="interest_table">
					<thead>
					 <tr>
						<th>Installment</th>
						<th>EMI S.No.</th>
						<th>Yr.Month</th>
						<th>Interest</th>
						<th>Principal</th>
						<th>Balance</th>
					 </tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<div id="feature_buttons" align="right">
				<button class="btn btn-primary emi-calculator" id="print_btn" type="button">Print Chart</button>
				<!-- <button class="btn btn-primary" id="save_btn" type="button" onclick="return false;">Save as Excel Sheet</button> -->
				</div>
			</div>
			<br />
        </div>

        <div class="span3">
        	<? include 'more_calculators.php' ?>
       </div>
      </div>
	<hr />
	<? include 'footer.php'; ?>
    </div> <!-- /container -->

