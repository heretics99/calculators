<?php 
include 'topnav.php'
?>
<script src="static/js/calculators.js"></script>
<link href="static/css/calculator_css.css" rel="stylesheet" />
<title>Monthly Recurring Deposit Calculator</title>
    <div class="container">

      <!-- Example row of columns -->
      <div class="row atleast-minheight">
        <div class="span9" id="calculator_div">
        	<h2 align="center" id="calculator_heading">Monthly Recurring Deposit Calculator</h2>
			<br />
			<br />
			<form class="form-horizontal">
				<div class="control-group">
				    <label class="control-label" for="principal">Monthly Installment</label>
				    <div class="controls">
				      <input type="text" id="principal" placeholder="Recurring Deposit Amt">
				    </div>
				</div>

				<div class="control-group">
				    <label class="control-label" for="rate">Rate of Interest</label>
				    <div class="controls">
				      <input type="text" id="rate" placeholder="% Rate of Interest">
				    </div>
				</div>

				<div class="control-group">
				    <label class="control-label" for="time">Time (months)</label>
				    <div class="controls">
				      <input type="text" id="time_years" placeholder="Years" class="input-small">
				      <input type="text" id="time_months" placeholder="and Months" class="input-small">
				      <span class="help-inline">Ex: 5 years and 6 months</span>
				    </div>
				</div>
				<div class="control-group">
				    <label class="control-label" >Compounded</label>
				    <div class="controls" style="white-space:nowrap">
					  <label class="radio"><input type="radio" name="compoundperiod" id="monthly" value="1" checked> Monthly </label>
					  <label class="radio"><input type="radio" name="compoundperiod" id="quarterly" value="3" > Quarterly </label>
					  <label class="radio"><input type="radio" name="compoundperiod" id="biannually" value="6" > Bi-Annually </label>
					  <label class="radio"><input type="radio" name="compoundperiod" id="annually" value="12"> Annually</label>
				    </div>
				</div>
				<div class="control-group">
					<label class="control-label" ></label>
				    <div class="actions">
				      <button class="btn btn-primary monthly-recurring" id="submit_btn" type="submit" onclick="return false;">Calculate</button>
				      <button class="btn" id="reset_btn" type="button" onclick="return false;">Reset</button>
				    </div>
				</div>
            </form>
			<div id="breakup_details" style="display: none">
				<hr/>
					<div class="well" id="raw_details">
				    <span class="" id="label4"></span> &nbsp;|&nbsp;
				    <span class="" id="label5"></span> &nbsp;|&nbsp;
				    <span class="" id="label6"></span>
				    </div>
					<div class="alert alert-success" id="brief_details">
				    <span class="badge badge-info bigtext" id="label1"></span> &nbsp;
				    <span class="badge badge-success bigtext" id="label2"></span> &nbsp;
				    <span class="badge badge-warning bigtext" id="label3"></span>
				    </div>
				<hr/>
				<div id="feature_buttons" class="pull-right button-padding">
				<button class="btn btn-primary recurring-deposit" id="print_btn" type="button">Print Chart</button>
				<!-- <button class="btn btn-primary" id="save_btn" type="button" onclick="return false;">Save as Excel Sheet</button> -->
				</div>
				<h3>Breakup Chart</h3>
				<table class="table table-hover table-bordered table-condensed" id="interest_table">
					<thead>
					 <tr>
						<th>Deposit</th>
						<th>Start</th>
						<th>End</th>
						<th>Total Time</th>
						<th>Total Amount</th>
						<th>Cumulative Sum</th>
					 </tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<div id="feature_buttons" align="right">
				<button class="btn btn-primary recurring-deposit" id="print_btn" type="button">Print Chart</button>
				<!-- <button class="btn btn-primary" id="save_btn" type="button" onclick="return false;">Save as Excel Sheet</button> -->
				</div>
			</div>
			<br />
        </div>

        <div class="span3">
        	<? include 'more_calculators.php' ?>
       </div>
      </div>
	<hr />
	<? include 'footer.php'; ?>
    </div> <!-- /container -->
