<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="Prakhar K Goel">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Style (aka, calling Bootstrap) -->
    <link href="static/css/bootstrap.min.css" rel="stylesheet">
    <link href="static/css/bootstrap-responsive.min.css" rel="stylesheet">
    <!-- <link href="{{STATIC_URL}}css/bootstrap-1.1.1.css" rel="stylesheet"> -->

    <!-- Scripts, inc. TableSorter -->
    <script src="http://code.jquery.com/jquery-1.5.2.min.js"></script>
    <!-- <script src="{{STATIC_URL}}js/bootstrap-popover.js"></script> -->
    <!-- <script src="{{STATIC_URL}}js/bootstrap-tooltip.js"></script> -->
    <!-- <script src="{{STATIC_URL}}js/bootstrap.min.js"></script> -->
	<meta property="fb:admins" content="1283418127"/>
	<meta property="fb:app_id" content="476267799070193"/>
	<meta property="og:title" content="Easy EMI | Recurring Deposits Calulator"/>
	<meta property="og:type" content="website"/>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-24662139-1', 'prakhargoel.com');
      ga('send', 'pageview');

    </script>
    
  </head>
</html>
